#! /usr/bin/env python3

import argparse
import math
import random


def getRandomGroupsOfFixedSize(popSize, groupSize, startAt=0):
	""" Generates a random repartition of a population into a list of (almost) same size groups. 
	
	Keyword arguments:
	popSize -- the population size, i.e. the number of elements to be scattered into the groups
	groupSize -- the number of population elements in each group. All the groups are composed of exactly groupSize elements, except the last one.
	startAt -- the index of the first element of the population (default 0)
	"""
	popul = list(range(startAt, popSize+startAt))
	random.shuffle(popul)
	#return [popul[groupSize*i:min(popSize,groupSize*(i+1))] for i in range(int(math.ceil(popSize/groupSize)))] # python3
	return [popul[groupSize*i:min(popSize,groupSize*(i+1))] for i in range(int(math.ceil(float(popSize)/groupSize)))] # python2



if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Generates a random repartition of a population into a list of (almost) same size groups")
	parser.add_argument("popSize", type=int, help="the population size, i.e. the number of elements to be scattered into the groups")
	parser.add_argument("groupSize", type=int, help="the number of population elements in each group. All the groups are composed of exactly groupSize elements, except the last one")
	parser.add_argument("-s", "--startsAt", type=int, help="the index of the first element of the population (default 0)")
	args=parser.parse_args()

	if args.startsAt:
		print(getRandomGroupsOfFixedSize(args.popSize, args.groupSize, args.startsAt))
	else:
		print(getRandomGroupsOfFixedSize(args.popSize, args.groupSize))

