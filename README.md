# randomGroups

Generates a random repartition of a population into a list of (almost) same size groups. Exists in both command line and graphical user interface versions.

## Usage

`randomGroups.py [-h] [-s STARTSAT] popSize groupSize`

![randomGroups input window](./images/randomGroups-cli.png)

### Positional arguments

- **popSize:** the population size, i.e. the number of elements to be scattered into the groups
- **groupSize:** the number of population elements in each group. All the groups are composed of exactly groupSize elements, except the last one

### Optional arguments

- **-h**, **--help** show the help message and exit
- **-s <startValue>**, **--startsAt <startValue>** the index of the first element of the population (default 0)

## Installation

- (install python)
- [download randomGroups.py and randomGroupsGUI.py](https://gitlab.com/odameron/randomGroups) 
- install [Gooey](https://github.com/chriskiehl/Gooey)

## GUI

Use `randomGroupsGUI.py` (inspired by [a bioinfo-fr.net article](https://bioinfo-fr.net/ajoutez-une-interface-graphique-a-votre-script-en-4-lignes-avec-gooey))

![randomGroups input window](./images/randomGroups-input.png)
![randomGroups result window](./images/randomGroups-result.png)

## Todo

- [X] add a GUI ([Gooey?](https://github.com/chriskiehl/Gooey))
- [ ] provide a list of idents insteads of integers (either as a tsv string or a file)

