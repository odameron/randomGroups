#! /usr/bin/env python3

import argparse
import math
import random
import randomGroups
from gooey import Gooey

# inspired by https://bioinfo-fr.net/ajoutez-une-interface-graphique-a-votre-script-en-4-lignes-avec-gooey

@Gooey
def main():
	parser = argparse.ArgumentParser(description="Generates a random repartition of a population into a list of (almost) same size groups")
	parser.add_argument("popSize", type=int, help="the population size, i.e. the number of elements to be scattered into the groups")
	parser.add_argument("groupSize", type=int, help="the number of population elements in each group. All the groups are composed of exactly groupSize elements, except the last one")
	parser.add_argument("-s", "--startsAt", type=int, help="the index of the first element of the population (default 0)")
	args=parser.parse_args()

	if args.startsAt:
		print(randomGroups.getRandomGroupsOfFixedSize(args.popSize, args.groupSize, args.startsAt))
	else:
		print(randomGroups.getRandomGroupsOfFixedSize(args.popSize, args.groupSize))

if __name__ == "__main__":
	main()

